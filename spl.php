<?php
include 'helpers.php';
/*
class Test
{
	public $a = 'a value';
	private $b = 'b value';
	protected $c = 'c value';
	function __construct(){}
	public function method_a(){
		echo 'public method';
	}
	private function method_b(){
		echo 'private method';
	}
	protected function method_c(){
		echo 'protected method';
	}
}

$test = new Test();
$array = new ArrayObject($test, ArrayObject::STD_PROP_LIST);
vd($array);
*/

/*
function gen(){
	while (true) {
		yield 1;
	}
}
$gen = gen();
vd(iterator_to_array($gen)); //memory limit
*/

/*
function gen(){
	$range = range(1,10);
	while (list($key, $val) = each($range)) {
		yield $val;
	}
}
$a = gen();
foreach ($a as $key => $value) {
	echo $value;
}
echo "<br>";
$a->rewind();
foreach ($a as $key => $value) {
	echo $value;
}
//exception throwed
*/

/*
function &gen(){
	$val = 3;
	while($val > 0){
		yield $val;
	}
}
foreach (gen() as &$value) {
	echo $value--;
}*/


/*
function gen(){
	while(true){
		$rand = rand(1,100);
		yield $rand;
		echo $rand;
		if($rand == 87)
			return;
		
	}
}
$gen = gen();
$arr = iterator_to_array($gen);
//$gen->rewind(); //throws error
foreach ($gen as $value) { //throws error also
	# code...
}
*/

/*
$gen = (function() {
    yield 1;
    yield 2;
    return 3;
})();

foreach ($gen as $val) {
    echo $val, PHP_EOL;
}

echo $gen->getReturn(), PHP_EOL;
*/


class MyClass{
	private $a = 1;
	protected $b = 2;
	public $c = 3;
	public function method(){
		return 'method';
	}
}
$obj = new MyClass();
$arr = [1, 2, 3, 4, 5];
$arrObj = new ArrayObject($arr);
unset($arrObj[2]);
vd($arrObj);
//$arrIt = $arrObj->getIterator();
//$arrIt = new ArrayIterator($arr);
//echo $arrObj["\0MyClass\0a"];
/*
foreach ($arrIt  as $key => $value) {
	vd($key.' '.$value);
	unset($arrIt[$key]);
}
vd($arrIt);
*/
