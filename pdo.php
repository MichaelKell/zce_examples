<?php
include 'helpers.php';
/*
https://rockmeamadeus4.wordpress.com/2010/09/06/the-use-of-foreach-with-the-pdostatement-class/


If you want to use PDO::FETCH_CLASS you need to set it up with setFetchMode first like so:
        $stmt->setFetchMode( PDO::FETCH_CLASS, 'classType', array( 'parameters to constructor' );
        $object = $stmt->fetch( PDO::FETCH_CLASS );
If you ommit this PHP will segfault. 


*/


$dsn = 'mysql:host=localhost;dbname=zce';
$user = 'mysql';
$pass = 'mysql';
$pdo = new PDO($dsn, $user, $pass);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
class Test{
	public $props = [];
	public function __construct(){
		echo 'construct<br>';
	}
	public function __set($prop, $val){
		$this->props[$prop] = $val;
		echo '__set';
	}
}

class Test2{
	public $id;
	public $email;
	public $name;
}

/*
$statement = $pdo->prepare('SELECT * FROM Users');
$statement->execute();
$statement->bindColumn('id', $col);
vd($statement->fetchAll(PDO::FETCH_ASSOC));
vd($col);
*/





$statement = $pdo->prepare('SELECT * FROM Users');
$statement->execute();
vd($statement->rowCount());
$statement->setFetchMode(PDO::FETCH_BOTH);
$res = $statement->fetch();



/*
foreach ($pdo->query('SELECT * FROM Users') as $key => $value) {
	echo '- '.$key.' '.$value.'<br>';
}
*/
/*
$statement = $pdo->prepare('SELECT * FROM Users');
$statement->execute();
foreach ($statement as $key => $value) {
	vd($value);
}
*/



/*
$statement = $pdo->query('SELECT * FROM Users');
$obj = new stdClass();
$obj1 = $statement->fetchObject($obj);
vd($obj1->email);
*/


$object = new stdclass();
/*
$statement = $pdo->query('SELECT id, email, name FROM Users', PDO::FETCH_INTO, $object);
$statement->execute();
$result = $statement->fetchAll();
*/
$statement = $pdo->prepare('SELECT id, email, name FROM Users');
/*
$statement->setFetchMode(PDO::FETCH_INTO, $object );
$statement->execute();
$result = $statement->fetchAll();
*/
/*	
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_INTO, $object); // fail
*/
/*
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_CLASS, 'stdclass');// ok with desired result
*/
$col = 0;
$statement->bindColumn(2, $col);
$statement->setFetchMode(PDO::FETCH_OBJ);
$statement->execute();
$result = $statement->fetchAll();


$limit = 10;
$offset = 0;
$st = $pdo->prepare("SELECT * FROM Users ORDER BY id LIMIT :limit OFFSET :offset");
$st->bindParam('limit', $limit, PDO::PARAM_INT);
$st->bindParam('offset', $offset, PDO::PARAM_INT);
$st->execute();
/*
$st->execute([
	':limit' => 10,
	':offset' => 0
]);
*/
var_dump($st->fetchAll());