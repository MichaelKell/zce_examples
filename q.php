<?php
include 'helpers.php';
/*
$str = '╒ one of the following';
echo str_replace('╒', 'Check', $str);	//replaced okay
*/
class Foo Implements ArrayAccess {
function offsetExists($k) {  return true;}
function offsetGet($k) { echo "string"; return 'a';}
function offsetSet($k, $v) {}
function offsetUnset($k) {}
}
$x = new Foo();
echo array_key_exists('foo', $x)?'true':'false';


/*
class Test {
	public function __call($name, $args)
	{
		call_user_func_array(array('static', "test$name"), $args);
	}
	public function testS($l) {
		echo "$l,";
	}
}
class Test2 extends Test {
	public function testS($l) {
		echo "$l,$l,";											// <- this called
	}
}
$test = new Test2();
$test->S('A');
*/


/*
class A {
public $a = 1;
public function __construct($a) { $this->a = $a; }
public function mul() {
return function($x) {
return $this->a*$x;
};
}
}
$a = new A(2);
$a->mul = function($x) {
return $x*$x;
};
vd($a);
$m = $a->mul();
echo $m(3);
*/


/*
trait MyTrait {
private $abc = 1;
public function increment() {
$this->abc++;
}
public function getValue() {
return $this->abc;
}
}
class MyClass {
use MyTrait;
public function incrementBy2() {
$this->increment();
$this->abc++;
}
}
$c = new MyClass;
$c->incrementBy2();
var_dump($c->getValue());
*/
/*
class Base {
	protected static function whoami() {
		echo "Base ";
	}
	public static function whoareyou() {
		static::whoami();
	}
}

class A extends Base {
	public static function test() {
		Base::whoareyou();
		self::whoareyou();
		parent::whoareyou();
		A::whoareyou();
		static::whoareyou();
	}
	public static function whoami() {
		echo "A ";
	}
}

class B extends A {
	public static function whoami() {
		echo "B ";
	}
}
B::test();
//base A base A B
*/
/*
function fibonacci (&$x1 = 0, &$x2 = 1)
{
$result = $x1 + $x2;
$x1 = $x2;
$x2 = $result;
return $result;
}
for ($i = 0; $i < 10; $i++) {
echo fibonacci() . ',';
}
*/

//echo "1" + 2 * "0x02";

//echo "22" + "0.2", 23 . 1;

/*
$text = 'This is text';
$text1 = <<<'TEXT'
$text
TEXT;
$text2 = <<<TEXT
$text1
TEXT;
echo "$text2";

//output: '$text'
*/


/*
try {
	class MyException extends Exception {};
	try {
		throw new MyException;
	}
	catch (Exception $e) {
		echo "1:";
		throw $e;
	}
	catch (MyException $e) {
		echo "2:";
		throw $e;
	}
}
catch (Exception $e) {
	echo get_class($e);
}
//output: 1:MyException
*/

/*
class Bar {
private $a = 'b';
public $c = 'd';
}
$x = (array) new Bar();
echo array_key_exists('a', $x) ? 'true' : 'false';
echo '-';
echo array_key_exists('c', $x) ? 'true' : 'false';
*/



/*
$array = array(1,2,3);
while (list(,$v) = each($array));
var_dump(current($array));	//bool(false)
*/


/*
function z($x) {
return function ($y) use ($x) {
return str_repeat($y, $x);
};
}
$a = z(2);
$b = z(3);
echo $a(3) . $b(2);
*/



/*
abstract class Graphics {
	abstract function draw($im, $col);
}
abstract class Point1 extends Graphics {
	public $x, $y;
	function __construct($x, $y) {
		$this->x = $x;
		$this->y = $y;
	}
	function draw($im, $col) {
		ImageSetPixel($im, $this->x, $this->y, $col);
	}
}
class Point2 extends Point1 { }
abstract class Point3 extends Point2 { }
new Point2(1, 2); // the only class we can instantiate here
*/


//echo md5(rand(), TRUE);