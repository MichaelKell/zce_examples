<?php
 $products = array(
        0 => array(
            'product_id' => 33,
            'variation_id' => 0,
            'product_price' => 500.00
        ),
        1 => array(
            'product_id' => 45,
            'variation_id' => 0,
            'product_price' => 600.00
        ),
2 => array(
            'product_id' => 48,
            'variation_id' => 0,
            'product_price' => 600.00
        ),
        3 => array(
            'product_id' => 49,
            'variation_id' => 0,
            'product_price' => 600.00
        )
    );

$missingItems= array(48);
$result = array_filter($products, function($val) use ($missingItems){
	return !in_array($val['product_id'], $missingItems);
});
var_dump($result);