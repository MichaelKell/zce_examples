<?php // GET LOST

/*
$array = [
	0 => 'zero',
	'0' => 'zero',
	0.8 => '0.8',
	'0.8' => '0.8',
	-2 => 'minus 2',
	'-2' => 'minus 2',
];

vd($array);
*/

/*
$array = [0,1,2,3];
unset($array[3]);
$array[] = 4;
vd($array);
*/

/*
$array = array(1, 2, 3, 4, 5);

foreach ($array as $i => $value) {
    unset($array[1]);
}
print_r($array);
*/


class A {
    private $A = 'private A of A'; // This will become '\0A\0A'
    protected $B = 'protected B of A'; // This will become '\0A\0A'
}
class B extends A {
    private $A = 'private A of B'; // This will become '\0B\0A'
    public $AA = 'public AA of B'; // This will become 'AA'
}
$test = (array) new B();
vd($test);
echo $test["\0A\0A"];
echo $test["*B"];




function vd($text){
  echo "<pre>";
  highlight_string(var_dump($text));
  echo "</pre>";
}

