<?php
function errorhandler($errno, $errstr, $errfile, $errlin){
	echo $errstr;
}

set_error_handler('errorhandler', E_ALL & ~E_NOTICE & ~E_USER_NOTICE);
trigger_error('err', E_RECOVERABLE_ERROR);