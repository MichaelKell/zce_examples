<?php
include 'helpers.php';


/*
$array = ['a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5];
//$iterator = new ArrayIterator($array, ArrayObject::STD_PROP_LIST|ArrayObject::ARRAY_AS_PROPS);
$iterator = new ArrayIterator($array);
$iterator->a = 2;
vd($iterator);
$iterator->setFlags(ArrayIterator::STD_PROP_LIST);
$iterator->a = 4;
vd((array)($iterator));
*/
/*
$iterator = new ArrayIterator(['a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5, 99 => 99]);
$iterator->setFlags(ArrayIterator::ARRAY_AS_PROPS);
echo $iterator->getFlags();
*/

/*
$pizzas   = new ArrayIterator(array('Margarita', 'Siciliana', 'Hawaii'));
$toppings = new ArrayIterator(array('Cheese', 'Anchovies', 'Olives', 'Pineapple', 'Ham'));
$appendIterator = new AppendIterator;
//$appendIterator->append($pizzas);
$appendIterator->append($toppings);
foreach ($appendIterator->getArrayIterator() as $key => $value) {
	vd($key);
	vd($value);
}
*/


/*
$ao = new ArrayObject();fg
$ao ->setFlags(ArrayObject::STD_PROP_LIST|ArrayObject::ARRAY_AS_PROPS);
$ao->prop = 'prop data';
$ao['arr'] = 'array data';
vd($ao);
*/

/*
$a = new ArrayObject(array(), ArrayObject::STD_PROP_LIST);
    $a['arr'] = 'array data';                            
    $a->prop = 'prop data';                              
$b = new ArrayObject();                                  
    $b['arr'] = 'array data';                            
    $b->prop = 'prop data';                                                                                
print_r($a);
echo "<br>";                                                                                                
print_r($b);                                
*/


/*
$it = new RegexIterator(new ArrayIterator(['dima bolkov', 'andrey golubev', 'kostya lomov', 'dima mishin', 'fraer lol', 'dima snova']), '/\s/', RegexIterator::SPLIT);
foreach ($it as $key => $value) {
  vd($key);
  vd($value);
}*/


$it = new ArrayIterator([0,1,2,3,4,5,6]);
while(list($key,$value) = each($it)){
  echo key($it);
   echo "-----------------";
  echo current($it);
 
  echo "<br>";
}



/*
$hey = [
          'Homepage',
          'Register',
          'About', [
                      'The Team',
                      'Our Story'
                    ],
          'Contact', [
                        'Locations',
                        'Support'
                      ]
        ];
$awesome = new RecursiveTreeIterator(
    new RecursiveArrayIterator($hey), 
    RecursiveTreeIterator::BYPASS_KEY, null, RecursiveIteratorIterator::SELF_FIRST
);
echo "<pre>";
foreach ($awesome as $key => $line){
  vd($awesome->getEntry());
 //var_dump($awesome->key());
 //var_dump($key);
 //var_dump($awesome->current());
 //var_dump($line);
 echo "----";
 //echo $key.PHP_EOL;
}
  //  
echo "</pre>";
*/

/*
$iterator = new DirectoryIterator('directoryiterator_directory');
//$iterator->seek(5);
//vd($iterator->getPathName());
foreach ($iterator as $key => $value) {
  vd($iterator);
  vd($value);
  echo '--------------------';
}
$iterator = new FilesystemIterator('directoryiterator_directory');
vd($iterator->getFlags());
foreach ($iterator as $key => $value) {
  vd($key);
  vd($value);
}*/






/*
$hey = array(
    'Web',
    array(
        'Search Engines',
        array('Google', 'Bing', 'Yahoo')
    ),
    array(
        'Social Coding',
        array('GitHub', 'Forrst')
    )
);
$awesome = new RecursiveIteratorIterator(new RecursiveArrayIterator($hey));
foreach ($awesome as $key => $value) {
	vd($key.' '.$value);
	vd($awesome->getSubIterator(0));
	vd('--------------------------------------------------------');
	vd('--------------------------------------------------------');
}
*/


/*
$awesome = new ArrayIterator([0, 1, 2, 3, 4, 5, 6, 7]);
foreach ($awesome as $key => $value) {
	vd($value);
	gonext($awesome);
}
function gonext($iterator){
	//vd("* ".$iterator->current());
	foreach ($iterator as $key => $value) {
		vd(' --'.$value);
	}
}*/

/*
$array = [0, 1, 2, 3, 4, 5, 6, 7];
foreach ($array as $key => $value) {
	vd(unset($array));
}

next($array);
foreach ($array as $key => $value) {
	vd(current($array));
}
*/



/*
flags:
http://stackoverflow.com/questions/12469220/what-reason-of-using-arrayobjectstd-prop-list
http://stackoverflow.com/questions/14610307/spl-arrayobject-arrayobjectstd-prop-list
*/

/*
http://paulyg.github.io/blog/2014/06/03/directoryiterator-vs-filesystemiterator.html
*/
/*
https://adayinthelifeof.nl/2014/02/12/spl-deepdive-regexiterator/
*/

/*
The main difference between these 2 is in the methods the classes have.

The ArrayIterator implements Iterator interface which gives it methods related to iteration/looping over the elements. ArrayObject has a method called exchangeArray that swaps it's internal array with another one. Implementing a similar thing in ArrayIterator would mean either creating a new object or looping through the keys & unseting all of them one by one & then setting the elements from new array one-by-one.

Next, since the ArrayObject cannot be iterated, when you use it in foreach it creates an ArrayIterator object internally(same as arrays). This means php creates a copy of the original data & there are now 2 objects with same contents. This will prove to be inefficient for large arrays. However, you can specify which class to use for iterator, so you can have custom iterators in your code.

ArrayIterator iterator allows to unset and modify values and keys while iterating over Arrays and Objects.

When you want to iterate over the same array multiple times you need to instantiate ArrayObject and let it create ArrayIterator instances that refer to it either by using foreach or by calling its getIterator() method manually.
*/