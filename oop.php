<?php
//http://stackoverflow.com/questions/39056873/access-to-private-properties-of-a-child-class
include 'helpers.php';
//http://stackoverflow.com/questions/3754786/calling-non-static-method-with
//http://stackoverflow.com/questions/5061476/php-forward-static-call-vs-call-user-func
//https://habrahabr.ru/post/259627/
https://jeremycook.ca/2010/09/05/some-tricks-with-autoloaders/
error_reporting(E_ALL);

/*
interface A{
    //public function Something(Array $a);
    //const R = 22;
}
interface B extends A{ // no overrirde methods nor constants
    //public function Something(stdClass $b);
    //const R = 10;
}
*/

trait A{
    public $a = 1;
}
trait B{
    public $a = 2;
}
class Test{
    use A,B;
}

echo (new Test)->a;

/*
class A {
    static function foo() {
        forward_static_call('B::bar'); //non-forwarding, 'B'
        call_user_func('B::bar'); //non-forwarding, 'B'
    }
}
class B extends A {
    static function bar() { echo get_called_class(), "\n"; }
}
A::foo();*/
/*
class A {
    protected static function foo() {
        echo "success!\n";
    }
    public static function test() {
        static::foo();
    }
}
class B extends A {
   // foo() will be copied to B, hence its scope will still be A and the call be successful 
}
class C extends A {
    protected static function foo() {
        // original method is replaced; the scope of the new one is C 
    }
}

B::test();
C::test();   //fails

*/


/*
class Test implements Serializable{
    public function serialize(){

    }
    public function unserialize($serialized){

    }
    public function __sleep(){

    }
}

$obj = new Test();
*/

/*
class Test implements Serializable{
    private $a = 'something';
    private $b = 'andmore';
    public function serialize(){
        return json_encode(['a' => $this->a, 'b' => $this->b]); 
    }
    public function unserialize($serialized){
        vd($serialized);
        $arr = json_decode($serialized, true);
        $this->a = $arr['a'];
        $this->b = $arr['b'];
    }

    public function __sleep(){
        return ['a'];
    }
}

$obj = new Test();
$serialized = serialize($obj);
vd(unserialize($serialized));
*/



/*
class ParentClass{
    private $c = 'ParentClass';

}
class ChildClass extends ParentClass{
    public function test(){
        echo $this->c;
    }

}
$obj = new ChildClass();
$obj->test();
*/

/*
$a = 1;
class B{
    const a = [1,2,3];
}
class A{
    public $a = B::a+1;
}
*/

/*
class A {
  private $foo="private a";
  protected $bar="protected a";
  function getAstuff() {
    return $this->foo.'/'.$this->bar;
  }
}
class B extends A {
  private $foo="private b";
  protected $bar="protected b";
  function getBstuff() {
    return $this->foo.'/'.$this->bar;
  }
}
$b=new A();
echo 'getBstuff: '.$b->getBstuff()."<br>getAstuff: ".$b->getAstuff()."\n";
*/

/*
class A {
    public static function who() {
        echo __CLASS__;
    }
    public static function test() {
        echo __CLASS__;                                                     // !!!
    }
}

class B extends A {
    public static function who() {
        echo __CLASS__;
    }
}

B::test();
*/


/*
class A {
    public static function whoareyou() {
        static::whoami();
    }
    public static function whoami() {
        echo "A";
    }
}

class B extends A {
    public static function test() {
        parent::whoareyou();
        self::whoareyou();
    }
    public static function whoami() {
        echo "B ";
    }
}

B::test();
*/



/*
class A {
    public static function resolve() {
        echo "A called";
    }
}
class B extends A {
    public static function resolve() {
        echo "B called";
    }
    public function test() {
        parent::resolve();
        self::resolve();
        A::resolve();
        B::resolve();
    }
}
(new B)->test();
*/


/*
$obj = (object) array('1' => 'foo');
var_dump(isset($obj->{'1'})); // outputs 'bool(false)'
var_dump(key($obj)); // outputs 'int(1)'
echo $obj->{'1'};
*/

/*
$test = (object)[1,2,3,4,5,6];
foreach ($test as $key => $value) {
    echo $key.' '.$value.'<br>';
}






/*
//if __set exists b will not be setted, else it will
class ClassName{
    public function __set($name, $v){
        echo "call";
    }
}
$class = new ClassName;
$class->b = 20; 
vd($class);
unset($class);
class ClassNameSecond{}
$class = new ClassNameSecond; 
$class->b = 20;
vd($class);
*/

/*
class Magic {
    protected $v = array('a' => 1, 'b' => 2, 'c' => 3);
    public function &__get($v) {
        return $this->v[$v];
    }
}
$m = new Magic();
$m->d[] = 4;
echo $m->d[0];
*/



/*
class A {                                                                                                                                                   //crazy stuff there
    public function test()
    {
        echo $this->name;
    }
}
class C {
     public function q()
     {
         $this->name = 'hello';
         A::test();
     }
}
$c = new C;
$c->q();
*/

/*
class Base {
    protected static function whoami() {
        echo "Base ";
    }
    public static function whoareyou() {
        static::whoami();
    }
}

class A extends Base {
    public static function test() {
        Base::whoareyou();
        self::whoareyou();
        parent::whoareyou();
        A::whoareyou();
        static::whoareyou();
    }
    public static function whoami() {
        echo "A ";
    }
}

class B extends A {
    public static function whoami() {
        echo "B ";
    }
}

B::test();
*/
//Base B B A B