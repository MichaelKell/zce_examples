<?php

/*
namespace foo{
	class Xyz {}
	function abc () {}
}
namespace bar{
	class Xyz {}
	function abc () {}
}
namespace bar{
	use foo\Xyz;
	use foo\abc;
	new Xyz(); // instantiates \foo\Xyz
	new namespace\Xyz(); // instantiates \bar\Xyz
	abc(); // invokes \bar\abc regardless of the second use statement
	\foo\abc(); // it has to be invoked using the fully qualified name
}
*/

/*
namespace a{
	class b_class{
		public function __construct(){
			echo __NAMESPACE__.__CLASS__."<br>";
		}
	}
}
namespace b{
	include 'namespace_include.php';
	use \ArrayObject;
	use a\b_class;
}
*/


/*
namespace foo{
	class Xyz {}
	function abcd () {
		echo __NAMESPACE__;
	}
}

namespace bar{
	class Xyz {}
	function abc () {
		echo __NAMESPACE__;
	}
}

namespace bar{
	//use foo\Xyz;
	use foo;
	new Xyz(); // instantiates \foo\Xyz
	new namespace\Xyz(); // instantiates \bar\Xyz
	foo\abcd(); // invokes \bar\abc regardless of the second use statement
	\foo\abcd(); // it has to be invoked using the fully qualified name
}
*/


/*
//best examle importing
namespace A\B{
	class c_class{
		public function __construct(){
			echo __CLASS__."<br>";
		}
	}
	function c_function(){
		echo __FUNCTION__."<br>";
	}
	define('a\b\c_const', 'c_const<br>');	//note: a\b\ prefix
}

namespace whatever{
	use a\b;	//1 equals: use a\b as b;
	use a\b\c_class; //2
	use function a\b\c_function; //3
	use const a\b\c_const; //4
	new b\c_class(); //1
	b\c_function(); //1
	echo b\c_const; //1
	new c_class(); //2
	c_function(); //3
	echo c_const; //4
	#with aliasing
	use a\b as the_namespace; //1
	use a\b\c_class as the_class; //2
	use function a\b\c_function as the_function; //3
	use const a\b\c_const as the_const; //4
	new the_namespace\c_class(); //1
	the_namespace\c_function(); //1
	echo the_namespace\c_const; //1
	new the_class(); //2
	the_function(); //3
	echo the_const; //4
}
*/


/*
namespace a{
	$str = 'c\\test';
}

namespace b{
	new $str;
}

namespace b\c{
	class test{
		public function __construct(){
			echo __NAMESPACE__;
		}
	}
}
namespace c{
	class test{
		public function __construct(){
			echo __NAMESPACE__;
		}
	}
}
*/


/*
namespace blah{
	class foo{
		public static function name(){
			echo "blah name";
		}
	}
}

namespace foo{
	class foo{
		public static function name(){
			echo "foo name";
		}
	}
	use blah\blah as foo;
	$a = new name(); // instantiates "foo\name" class
	foo::name(); // calls static method "name" in class "blah\blah"
}
*/



/*
namespace foo\bar{
	class baz{
		public function hi(){
			echo "hi";
		}
	}
}

namespace whatever{
	use foo\bar\baz as coolClass;
	$obj = new coolClass();
	$obj->hi();
}
*/
/*
namespace{
	//define('FOO', 'hello');
}
namespace whatever{
	$a = FOO; //if undefined - assumed string, if not - fallback to global
	echo $a;
}
*/

/*
namespace{
	$var = 'global var';
}

namespace k\l{
  class MyClass{
  	public function hi(){
  		echo "k\l hi";
  	}
  }
}
namespace a\b{
  class MyClass{
  	public function hi(){
  		echo "a\b hi";
  	}
  }
}


namespace y {
  $GLOBALS['class'] = 'k\l\MyClass';
}


namespace z {
  use k\l as B;
  $classname = $GLOBALS['class'];
  $a = new $classname;
  $a->hi();
}*/


/*
namespace foo;
include 'namespace_include.php';
use blah\blah as foo;
//$a = new name(); // instantiates "foo\name" class
$b = new foo\bar();
$b->hi();
foo::name(); // calls static method "name" in class "blah\blah"
*/