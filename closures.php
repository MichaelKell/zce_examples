<?php
/*
class A{
    private $a = 1;
    protected $b = 2;
    public $c = 3;
    public function __construct(){}
    private function a(){
        return 'a';
    }
    private function b(){
        return 'b';
    }
    private function c(){
        return 'c';
    }
}
$closure = function(){
    echo $this->b;
};

$closure = $closure->bindTo(new A());
$closure();
*/


class A{
	private $a = 1;
	protected $b = 2;
	public $c = 5555;
}
class B extends A{
	private $a = 3;
	protected $b = 4;
}
$closure = function(){
	///echo $this->a;
	echo "<br>";
	echo $this->c;
};
$closure = $closure->bindTo(new B);
$closure();

















































/*
I've been trying to understand how closure scope really affects on which properties. . Just interesting. In particular case - what would happen if i set and then bind(or just pass through parameter) an object of a child class 
class ParentClass{
	public $public = 'public ParentClass';
	protected $protected = 'protected ParentClass';
	private $private = 'private ParentClass';
}

class ChildClass extends ParentClass{
	public $a = 'public ChildClass';
	protected $b =  'protected ChildClass';
	private $b =  'private ChildClass';
}

$closure = function(){
	echo $this->public, ', ', $this->protected, ', ', $this->private;
}
$closure = $closure->bindTo(new ChildClass(), ParentClass::class);
$closure();

Outputs: public ChildClass, protected ChildClass, private ParentClass
Maybe that's not how you really would use it. I just try to get some resolving rules I have absolutely no idea 

When i use scope of parent class isn't it the same as i would just call
class ParentClass{
	public $public = 'public ParentClass';
	protected $protected = 'protected ParentClass';
	private $private = 'private ParentClass';
	public function test(){
		
	}
}

class ChildClass extends ParentClass{
	public $a = 'public ChildClass';
	protected $b =  'protected ChildClass';
	private $b =  'private ChildClass';
}
*/